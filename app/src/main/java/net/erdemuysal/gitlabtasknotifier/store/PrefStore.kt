package net.erdemuysal.gitlabtasknotifier.store

import android.content.Context
import android.content.SharedPreferences
import net.erdemuysal.gitlabtasknotifier.pattern.SingletonHolder

class PrefStore private constructor(context: Context) {
    companion object : SingletonHolder<PrefStore, Context>(::PrefStore)

    private val prefStoreName = "prefStore"
    private var preferences: SharedPreferences

    init {
        preferences = context.getSharedPreferences(prefStoreName, Context.MODE_PRIVATE)
    }

    fun getPref(): SharedPreferences {
        return preferences
    }

    fun getEditor(): SharedPreferences.Editor {
        return preferences.edit()
    }
}