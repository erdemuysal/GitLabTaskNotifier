package net.erdemuysal.gitlabtasknotifier.store

import android.content.Context
import net.erdemuysal.gitlabtasknotifier.pattern.SingletonHolder


class GitLabURLStore private constructor(private val context: Context) {
    companion object : SingletonHolder<GitLabURLStore, Context>(::GitLabURLStore)

    private val urlKey = "GitLabURL"
    private val defaultURL = "https://gitlab.com"

    @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    fun getURL() : String {
        return PrefStore.getInstance(context).getPref().getString(urlKey, defaultURL)
    }

    fun writeURL(url : String){
        PrefStore.getInstance(context).getEditor().putString(urlKey, url).apply()
    }
}