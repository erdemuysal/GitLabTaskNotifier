package net.erdemuysal.gitlabtasknotifier.store

import android.content.Context
import net.erdemuysal.gitlabtasknotifier.pattern.SingletonHolder

class TokenStore private constructor(private val context: Context){
    companion object : SingletonHolder<TokenStore, Context>(::TokenStore)

    private val tokenKey = "GitLabAccessToken"

    fun getToken(): String? {
        return PrefStore.getInstance(context).getPref().getString(tokenKey, null)
    }

    fun writeToken(token: String) {
        PrefStore.getInstance(context).getEditor().putString(tokenKey, token).apply()
    }
}
