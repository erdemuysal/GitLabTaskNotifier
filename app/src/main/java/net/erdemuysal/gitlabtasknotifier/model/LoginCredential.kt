package net.erdemuysal.gitlabtasknotifier.model

data class LoginCredential(var url: String, var token: String?)