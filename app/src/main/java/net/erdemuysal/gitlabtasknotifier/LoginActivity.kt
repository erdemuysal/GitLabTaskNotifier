package net.erdemuysal.gitlabtasknotifier

import android.content.Intent
import android.databinding.DataBindingUtil
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_login.*
import net.erdemuysal.gitlabtasknotifier.databinding.LoginComponentBinding
import net.erdemuysal.gitlabtasknotifier.model.LoginCredential
import net.erdemuysal.gitlabtasknotifier.store.GitLabURLStore
import net.erdemuysal.gitlabtasknotifier.store.TokenStore
import net.erdemuysal.gitlabtasknotifier.ui.uibinder.LoginCredentialBinder
import net.erdemuysal.gitlabtasknotifier.util.isValidURL

class LoginActivity : AppCompatActivity(), LoginCredentialBinder.LoginComponentActions {

    private lateinit var loginCredential: LoginCredential
    private lateinit var loginCredentialBinder: LoginCredentialBinder

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        loginCredential = LoginCredential(GitLabURLStore.getInstance(this).getURL(), TokenStore.getInstance(this).getToken())
        loginCredentialBinder = LoginCredentialBinder(loginCredential, this)
        DataBindingUtil.bind<LoginComponentBinding>(loginComponent)?.setVariable(BR.loginCredentialBinder, loginCredentialBinder)
    }

    override fun onLoginButtonClicked() {
        if (loginCredentialBinder.loginCanProceed()) {
            GitLabURLStore.getInstance(this).writeURL(loginCredential.url)
            loginCredential.token?.let { TokenStore.getInstance(this).writeToken(it) }
        }
    }

    override fun onObtainTokenButtonClicked() {
        if (loginCredential.url.isValidURL()) {
            openWebBrowser("${loginCredential.url}/profile/personal_access_tokens")
        }
    }

    private fun openWebBrowser(url: String) {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        startActivity(browserIntent)
    }
}
