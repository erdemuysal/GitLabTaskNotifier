package net.erdemuysal.gitlabtasknotifier.store

import android.content.Context
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class TokenStoreTest {

    private val context: Context = InstrumentationRegistry.getTargetContext()
    private lateinit var tokenStore: TokenStore
    private val token = "TokenString"

    @Before
    fun setUp() {
        tokenStore = TokenStore.getInstance(context)
    }

    @Test
    fun whenTokenIsNotSetShouldReturnNull() {
        Assert.assertNull(tokenStore.getToken())
    }

    @Test
    fun whenTokenIsSetShouldReturnSameValue() {
        tokenStore.writeToken(token)
        Assert.assertEquals(token, tokenStore.getToken())
    }

    @Test
    fun whenTokenSetMultipleTimesShouldReturnLastValue() {
        for (i in 1..10) {
            tokenStore.writeToken(i.toString())
        }
        tokenStore.writeToken(token)
        Assert.assertEquals(token, tokenStore.getToken())
    }
}