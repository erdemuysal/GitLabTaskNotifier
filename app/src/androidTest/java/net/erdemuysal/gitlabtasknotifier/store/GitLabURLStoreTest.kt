package net.erdemuysal.gitlabtasknotifier.store

import android.content.Context
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class GitLabURLStoreTest {

    private val context: Context = InstrumentationRegistry.getTargetContext()
    private lateinit var urlStore: GitLabURLStore
    private val url = "testURL"
    private val defaultURL = "https://gitlab.com"

    @Before
    fun setUp() {
        urlStore = GitLabURLStore.getInstance(context)
    }

    @Test
    fun whenURLIsNotSetShouldReturnDefault() {
        Assert.assertEquals(defaultURL, urlStore.getURL())
    }

    @Test
    fun whenURLIsSetShouldReturnSameValue() {
        urlStore.writeURL(url)
        Assert.assertEquals(url, urlStore.getURL())
    }

    @Test
    fun whenURLSetMultipleTimesShouldReturnLastValue() {
        for(i in 1..10){
            urlStore.writeURL(i.toString())
        }
        urlStore.writeURL(url)
        Assert.assertEquals(url, urlStore.getURL())
    }
}